const Product = require('../models/Product')

const handleRead = async (req, res) => {
    const filter = req.body.filter || {}
    const sortBy = req.body.sortBy || {}

    const result = await Product.find(filter, null, { sort: sortBy }).exec()

    res.status(200).json({ products: result })
}

const handleCreate = async (req, res) => {
    const { name, price, description, quantity, unit } = req.body

    const duplicate = await Product.findOne({ name })
    if (duplicate) return res.status(409).json({ msg: 'Product with this name already exists' })

    const product = new Product({ name, price, description, quantity, unit })
    product.save()

    res.status(201).json({ msg: `Product ${name} added to the warehouse` })
}

const handleUpdate = async (req, res) => {
    const id = req.params.id

    const product = await Product.findById(id)
    if (!product) return res.status(400).json({ msg: 'Product not found' })

    const name = req.body.name || product.name
    const price = req.body.price || product.price
    const description = req.body.description || product.description
    const quantity = req.body.quantity || product.quantity
    const unit = req.body.unit || product.unit

    await Product.findByIdAndUpdate(id, { name, price, description, quantity, unit })

    res.status(200).json({ msg: `${product.name} was updated` })
}

const handleDelete = async (req, res) => {
    const id = req.params.id

    const product = await Product.findById(id)
    if (!product) return res.status(400).json({ msg: 'Product not found' })
    if (product.isRealised) return res.status(400).json({ msg: 'This product cannot be deleted' })


    await Product.findByIdAndDelete(id)

    res.status(200).json({ msg: `${product.name} was deleted` })
}

const handleReport = async (req, res) => {
    const result = await Product.aggregate([
        { $project: { 
            _id: 0,
            name: 1, 
            quantity: 1,
            total_worth:{ $multiply: ["$price", "$quantity"]}
        }}
    ])

    res.status(200).json({ report: result })
}

module.exports = { handleRead, handleCreate, handleUpdate, handleDelete, handleReport }
