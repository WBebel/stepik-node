require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const PORT = process.env.PORT || 3500
const app = express()

app.use(express.json())

mongoose.connect(process.env.DATABASE_URI)

const productController = require('./controllers/productController')

app.get('/products', productController.handleRead)
app.post('/products', productController.handleCreate)
app.put('/products/:id', productController.handleUpdate)
app.delete('/products/:id', productController.handleDelete)
app.get('/report', productController.handleReport)

app.listen(PORT, () => console.log(`Listening on port ${PORT}`))
